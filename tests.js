const chai = require("chai");
const chaiHttp = require("chai-http");

const app = require("./index");

// Configure chai
chai.use(chaiHttp);
chai.should();

const expect = chai.expect;

describe("Tests", () => {
    describe("GET /data-true", () => {
        // Test to get all students record
        it("should get true data", (done) => {
            chai.request(app)
                .get('/data-true')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    expect(res.body.success).to.equal(true);
                    done();
                });
        });
        // Test to get single student record
        it("should get false data", (done) => {
            chai.request(app)
                .get(`/data-false`)
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.should.be.a('object');
                    expect(res.body.success).to.equal(false);
                    done();
                });
        });
    });
});