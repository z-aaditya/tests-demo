const express = require("express");

const app = express();

app.get("/data-true", (req, res) => {
    res.status(200).json({
        success: true
    })
})

app.get("/data-false", (req, res) => {
    res.status(401).json({
        success: false
    })
})

app.listen(3000, () => {
    console.log("Listening at 3000.");
})

module.exports = app;